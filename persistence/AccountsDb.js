'use strict';

const config = require('../config-app.json');
const mLab = require('mongolab-data-api')(config.MLAB.API_KEY);

const _options = {
    database: config.MLAB.DATABASE,
    collectionName: config.MLAB.ACCOUNTS
};

exports.insertAccount = function(document, res) {
    let options = _options;
    document.active = true;
    document.id = 0;
    document.transactions = [];
    options.documents = document;
    mLab.insertDocuments(options, function (err, datai) {
      // Generate numeric id from mongo ObjectId
      let idNum = parseInt(datai._id['$oid'].toString().substr(0,8), 16);
      let idStr = idNum.toString().split("").reverse().join("");
      let reversedNum = parseInt(idStr);
      // Update numeric id
      delete options.documents;
      options.id = datai._id['$oid'];
      datai.id = reversedNum;
      options.updateObject = datai;
      mLab.updateDocument(options, function (err, datau) {
        // Retrieve final document with ObjectId
        delete options.id;
        delete options.updateObject;
        options.id = datai._id['$oid'];
        mLab.viewDocument(options, function (err, datav) {
            // Remove partial document
            delete datav._id;
            delete datav.transactions;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(datav));
        });
      });
    });
}

exports.getAllAccountsByCustomerId = function(id, res) {
    // Getting document by numeric id
    let options = _options;
    options.query = '{"customerId":'+id+'}';
    options.setOfFields = JSON.stringify({'_id':0,'id':1,'customerId':1,'description':1,'active':1});
    mLab.listDocuments(options, function (err, datac) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(datac));
    });
}

exports.insertTransaction = function(customerId, accountId, document, res) {
    let options = _options;
    let now = new Date();
    let nowIsoString = now.toISOString();
    document.date = nowIsoString; 
    // Getting mongo Id
    options.query = '{ $and: [ { customerId: { $eq: '+customerId+' } }, { id: { $eq: '+accountId+' } } ] }';
    options.findOne = true;
    options.setOfFields = JSON.stringify({'_id':1});
    mLab.listDocuments(options, function (err, datac) {
        // Add document to transactions array
        delete options.query;
        delete options.findOne;
        delete options.setOfFields;
        let oData = { $push: { transactions: document}};
        options.updateObject = oData;
        options.id = datac._id['$oid'];
        mLab.updateDocument(options, function (err, datau) {
            delete datau._id;
            delete datau.transactions;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(document));
        });
    });
}

exports.getAllTransactionsByAccountId = function(customerId, accountId, res) {
    // Getting documents from transactions array where customerId, accountId match
    let options = _options;
    options.query = '{ $and: [ { customerId: { $eq: '+customerId+' } }, { id: { $eq: '+accountId+' } } ] }';
    options.setOfFields = JSON.stringify({'transactions':1});
    options.findOne = true;
    mLab.listDocuments(options, function (err, datac) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(datac.transactions));
    });
}
