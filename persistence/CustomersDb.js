'use strict';

const bcrypt = require('bcrypt');
const config = require('../config-app.json');
const mLab = require('mongolab-data-api')(config.MLAB.API_KEY);

const _options = {
  database: config.MLAB.DATABASE,
  collectionName: config.MLAB.CUSTOMERS
};

const error = {
  "code": 503,
  "description": "",
  "reasonPhrase": ""
};

exports.delete = function(id, res) {
  // Getting document with numeric id, if exists
  let options = _options;
  options.query = JSON.stringify({'id':id});
  options.findOne = true;
  options.setOfFields = JSON.stringify({'_id':1});
  mLab.listDocuments(options, function (err, datac) {
    // If exists, delete document using Object Id
    delete options.query;
    delete options.findOne;
    delete options.setOfFields;
    options.id = datac._id['$oid'];
    mLab.deleteDocument(options, function (err, datad) {
      // no response value expected for this operation
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify({result:true}));
    });
  });
}

exports.getAll = function(res) {
  // Getting all documents from collection
  let options = _options;
  options.setOfFields = JSON.stringify({'_id':0,'security':0});
  mLab.listDocuments(options, function (err, data) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(data));
  });
}

exports.getById = function(id, res) {
  // Getting document by numeric id
  let options = _options;
  options.query = JSON.stringify({'id':id});
  options.findOne = true;
  options.setOfFields = JSON.stringify({'firstName':1,'lastName':1,'email':1,'active':1,'id':1});
  mLab.listDocuments(options, function (err, datac) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(datac));
  });
}

exports.getBySecuritySession = function(session, res) {
  // Getting document by security session
  let options = _options;
  options.query = '{"security.session":"'+session+'"}';
  options.findOne = true;
  options.setOfFields = JSON.stringify({'firstName':1,'lastName':1,'email':1,'active':1,'id':1});
  mLab.listDocuments(options, function (err, datac) {
    delete datac._id;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(datac));
  });
}

exports.insert = function(document, res) {
  // Generate temporal password
  const generator = require('generate-password');
  let temporalPassword = generator.generate({
      length: 10,
      numbers: true
  });
  let options = _options;
  // Hash original user password
  bcrypt.hash(document.password, config.PASSWORD.SALT_ROUNDS, function(err, hashOriginalPassword) {
    bcrypt.hash(temporalPassword, config.PASSWORD.SALT_ROUNDS, function(err, hashTemporalPassword) {
      // Insert complete document
      document.active = false;
      document.id = 0;
      document.security = {'password':hashOriginalPassword,'session':hashTemporalPassword};
      delete document.password;
      options.documents = document;
      mLab.insertDocuments(options, function (err, datai) {
        // Generate numeric id from mongo ObjectId
        let idNum = parseInt(datai._id['$oid'].toString().substr(0,8), 16)*1000;
        // Update numeric id
        delete options.documents;
        options.id = datai._id['$oid'];
        datai.id = idNum;
        options.updateObject = datai;
        mLab.updateDocument(options, function (err, datau) {
          // Retrieve final document with ObjectId
          delete options.id;
          delete options.updateObject;
          options.id = datai._id['$oid'];
          mLab.viewDocument(options, function (err, datav) {
            // Remove partial document
            delete datav._id;
            delete datav.security;
            // Send id and temporal password by email
            const requestJson = require('request-json');
            let client = requestJson.createClient(config.EMAIL.API_BASE);
            client.setBasicAuth(config.EMAIL.API_KEY, config.EMAIL.SECRET_KEY);
            let message = config.EMAIL.MESSAGE_TEMPLATE;
            message.Recipients = [{'Email':datav.email}];
            message['Text-part'] = '';
            message['Html-part'] = 'Customer Id: ' + datav.id + '<br>' + 'Activation password: ' + temporalPassword;
            console.log('temporalPassword['+datav.id+']: ' + temporalPassword);
            client.post(config.EMAIL.API_RESOURCE, message, function(errClient, resClient, bodyClient) {
              if(errClient){
                console.log(errClient);
              } else {
                console.log('Response send email['+datav.id+']: ' +JSON.stringify(bodyClient));
                // If all previous ok, response document
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify(datav));  
              }
            });
          });
        });  
      });  
    });
  });
}

exports.activate = function(customerId, temporalPassword, res) {
  // Getting document by id
  let options = _options;
  options.query = JSON.stringify({'id':customerId});
  options.findOne = true;
  options.setOfFields = JSON.stringify({'_id':1,'firstName':1,'lastName':1,'email':1,'active':1,'id':1,'security':1});
  mLab.listDocuments(options, function (err, datac) {
    bcrypt.compare(temporalPassword, datac.security.session, function(err, resultBcrypt) {
      if(resultBcrypt) {
        // Match. Then activate customer and remove session from security
        delete options.query;
        delete options.findOne;
        delete options.setOfFields;
        options.id = datac._id['$oid'];
        datac.active = true;
        datac.security.session = '';
        options.updateObject = datac;
        mLab.updateDocument(options, function (err, datau) {
          res.setHeader('Content-Type', 'application/json');
          res.end(JSON.stringify({result:true}));
        });
      } else {
        // Not match
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({result:false}));
      }
    });
  });
}

exports.updateById = function(id, newDocument, res) {
  let options = _options;
  // Get document with specific customerid
  options.query = JSON.stringify({'id':id});
  options.findOne = true;
  options.setOfFields = JSON.stringify({'_id':1,'firstName':1,'lastName':1,'email':1,'active':1,'id':1,'security':1});
  mLab.listDocuments(options, function (err, datac) {
    // Getting complete document
    let document = datac;
    // Only update required fields
    document.firstName = newDocument.firstName;
    document.lastName = newDocument.lastName;
    options.updateObject = document;
    delete options.query;
    delete options.findOne;
    options.id = datac._id['$oid'];
    mLab.updateDocument(options, function (err, datau) {
      // Remove partial document
      delete datau._id;
      delete datau.security;
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(datau));
    });
  });
}

exports.deleteSecuritySession = function(xAppSession, res) {
  // Getting document by security session
  let options = _options;
  options.query = '{"security.session":"'+xAppSession+'"}';
  options.findOne = true;
  options.setOfFields = JSON.stringify({'_id':1,'firstName':1,'lastName':1,'email':1,'active':1,'id':1,'security':1});
  mLab.listDocuments(options, function (err, datac) {
    // Remove security session property and update document
    delete options.query;
    delete options.findOne;
    delete options.setOfFields;
    options.id = datac._id['$oid'];
    datac.security.session = '';
    options.updateObject = datac;
    mLab.updateDocument(options, function (err, datau) {
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify({result:true}));
    });
  });
}

exports.insertSecuritySession = function(customerId, plainPassword, res) {
  // Getting document by customerId
  let options = _options;
  options.query = JSON.stringify({'id':customerId});
  options.findOne = true;
  options.setOfFields = JSON.stringify({'_id':1,'firstName':1,'lastName':1,'email':1,'active':1,'id':1,'security':1});
  mLab.listDocuments(options, function (err, datac) {
    if(datac.active) {
      // Is active. Check password
      bcrypt.compare(plainPassword, datac.security.password, function(err, resultBcrypt) {
        if(resultBcrypt) {
          // Match. Then create numeric session, hash and store in security. 
          bcrypt.hash(Date.now().toString(), config.PASSWORD.SALT_ROUNDS, function(err, hashSessionPassword) {
            delete options.query;
            delete options.findOne;
            delete options.setOfFields;
            options.id = datac._id['$oid'];
            datac.security.session = hashSessionPassword;
            options.updateObject = datac;
            mLab.updateDocument(options, function (err, datau) {
              res.setHeader('Content-Type', 'application/json');
              res.end(JSON.stringify({session:hashSessionPassword}));
            });  
          });
        } else {
          // Not match password. No session created.
          console.log('no match');
          res.statusCode = 503;
          error.description = 'Not match password. No session created.';
          res.end(JSON.stringify(error));
        }
      });
    } else {
      // Not active. No session created.
      res.statusCode = 503;
      error.description = 'Not active. No session created.';
      res.end(JSON.stringify(error));
    }
  });
}