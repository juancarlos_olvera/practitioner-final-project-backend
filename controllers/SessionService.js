'use strict';

exports.deleteSession = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * session (String)
  **/
  let xAppSession = args.session.value;
  customersDb.deleteSecuritySession(xAppSession, res);
}

exports.postSession = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * body (AnonymousRepresentation)
  **/
  let customerId = args.body.value.customerId;
  let plainPassword = args.body.value.password;
  customersDb.insertSecuritySession(customerId, plainPassword, res);
}

