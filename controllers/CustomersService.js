'use strict';

exports.deleteCustomersCustomerid = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * xAppSession (String)
  **/
  customersDb.delete(args.customerid.value, res);
}

exports.getCustomers = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * xAppSession (String)
  * $size (Long)
  * $page (Long)
  * $sort (String)
  * email (String)
  **/
  customersDb.getAll(res);
}

exports.getCustomersCustomerid = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * xAppSession (String)
  **/
  customersDb.getById(args.customerid.value, res);
}

exports.getCustomersMe = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * xAppSession (String)
  **/
  let xAppSession = args['X-App-Session'].value;
  customersDb.getBySecuritySession(xAppSession, res);
}

exports.postCustomers = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * xAppSession (String)
  * body (Customer)
  **/
  customersDb.insert(args.body.value, res);
}

exports.postCustomersActivate = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * body (AnonymousRepresentation3)
  **/
  // Getting id and temporal password
  let customerId = args.body.value.customerId;
  let temporalPassword = args.body.value.temporalPassword;
  customersDb.activate(customerId, temporalPassword, res);
}

exports.putCustomersCustomerid = function(args, res, next, customersDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * xAppSession (String)
  * body (Customer)
  **/
  let newDocument = args.body.value;
  let customerId = args.customerid.value;
  customersDb.updateById(customerId,newDocument, res);
}
