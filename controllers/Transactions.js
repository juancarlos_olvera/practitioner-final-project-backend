'use strict';

var url = require('url');


var Transactions = require('./TransactionsService');
var AccountsDb = require('../persistence/AccountsDb');


module.exports.deleteCustomersCustomeridAccountsAccountidTransactionsTransactionid = function deleteCustomersCustomeridAccountsAccountidTransactionsTransactionid (req, res, next) {
  Transactions.deleteCustomersCustomeridAccountsAccountidTransactionsTransactionid(req.swagger.params, res, next), AccountsDb;
};

module.exports.getCustomersCustomeridAccountsAccountidTransactions = function getCustomersCustomeridAccountsAccountidTransactions (req, res, next) {
  Transactions.getCustomersCustomeridAccountsAccountidTransactions(req.swagger.params, res, next, AccountsDb);
};

module.exports.getCustomersCustomeridAccountsAccountidTransactionsTransactionid = function getCustomersCustomeridAccountsAccountidTransactionsTransactionid (req, res, next) {
  Transactions.getCustomersCustomeridAccountsAccountidTransactionsTransactionid(req.swagger.params, res, next, AccountsDb);
};

module.exports.postCustomersCustomeridAccountsAccountidTransactions = function postCustomersCustomeridAccountsAccountidTransactions (req, res, next) {
  Transactions.postCustomersCustomeridAccountsAccountidTransactions(req.swagger.params, res, next, AccountsDb);
};

module.exports.putCustomersCustomeridAccountsAccountidTransactionsTransactionid = function putCustomersCustomeridAccountsAccountidTransactionsTransactionid (req, res, next) {
  Transactions.putCustomersCustomeridAccountsAccountidTransactionsTransactionid(req.swagger.params, res, next, AccountsDb);
};
