'use strict';

const config = require('../config-app.json');
const mLab = require('mongolab-data-api')(config.MLAB.API_KEY);

const _options = {
  database: config.MLAB.DATABASE,
  collectionName: config.MLAB.ACCOUNTS
};

exports.deleteCustomersCustomeridAccountsAccountid = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * accountid (Double)
  * xAppSession (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.getCustomersCustomeridAccounts = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (String)
  * xAppSession (String)
  * $size (Long)
  * $page (Long)
  * $sort (String)
  **/
  let customerId = args.customerid.value;
  accountsDb.getAllAccountsByCustomerId(customerId, res);
}

exports.getCustomersCustomeridAccountsAccountid = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * accountid (Double)
  * xAppSession (String)
  **/
  // TODO
  
}

exports.postCustomersCustomeridAccounts = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (String)
  * xAppSession (String)
  * body (Account)
  **/
  let dataAccount = args.body.value;
  let customerId = args.customerid.value;
  dataAccount.customerId = customerId;
  accountsDb.insertAccount(dataAccount, res);
}

exports.putCustomersCustomeridAccountsAccountid = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * accountid (Double)
  * xAppSession (String)
  * body (Account)
  **/
  
  //TODO
  
}

