'use strict';

var url = require('url');


var Accounts = require('./AccountsService');
var AccountsDb = require('../persistence/AccountsDb');


module.exports.deleteCustomersCustomeridAccountsAccountid = function deleteCustomersCustomeridAccountsAccountid (req, res, next) {
  Accounts.deleteCustomersCustomeridAccountsAccountid(req.swagger.params, res, next, AccountsDb);
};

module.exports.getCustomersCustomeridAccounts = function getCustomersCustomeridAccounts (req, res, next) {
  Accounts.getCustomersCustomeridAccounts(req.swagger.params, res, next, AccountsDb);
};

module.exports.getCustomersCustomeridAccountsAccountid = function getCustomersCustomeridAccountsAccountid (req, res, next) {
  Accounts.getCustomersCustomeridAccountsAccountid(req.swagger.params, res, next, AccountsDb);
};

module.exports.postCustomersCustomeridAccounts = function postCustomersCustomeridAccounts (req, res, next) {
  Accounts.postCustomersCustomeridAccounts(req.swagger.params, res, next, AccountsDb);
};

module.exports.putCustomersCustomeridAccountsAccountid = function putCustomersCustomeridAccountsAccountid (req, res, next) {
  Accounts.putCustomersCustomeridAccountsAccountid(req.swagger.params, res, next, AccountsDb);
};
