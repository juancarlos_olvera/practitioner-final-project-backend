'use strict';

exports.deleteCustomersCustomeridAccountsAccountidTransactionsTransactionid = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * accountid (Double)
  * transactionid (Double)
  * xAppSession (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.getCustomersCustomeridAccountsAccountidTransactions = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (String)
  * accountid (String)
  * xAppSession (String)
  * $size (Long)
  * $page (Long)
  * $sort (String)
  * type (String)
  **/
  let customerId = args.customerid.value;
  let accountId = args.accountid.value;
  accountsDb.getAllTransactionsByAccountId(customerId, accountId, res);
}

exports.getCustomersCustomeridAccountsAccountidTransactionsTransactionid = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * accountid (Double)
  * transactionid (Double)
  * xAppSession (String)
  **/
  
  // TODO
  
}

exports.postCustomersCustomeridAccountsAccountidTransactions = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (String)
  * accountid (String)
  * xAppSession (String)
  * body (Transaction)
  **/
  let dataTransaction = args.body.value;
  let customerId = args.customerid.value;
  let accountId = args.accountid.value;
  accountsDb.insertTransaction(customerId, accountId, dataTransaction, res);
}

exports.putCustomersCustomeridAccountsAccountidTransactionsTransactionid = function(args, res, next, accountsDb) {
  /**
   * parameters expected in the args:
  * customerid (Double)
  * accountid (Double)
  * transactionid (Double)
  * xAppSession (String)
  * body (Transaction)
  **/
  
  // TODO
  
}

