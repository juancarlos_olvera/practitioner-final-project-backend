'use strict';

var url = require('url');


var Session = require('./SessionService');
var CustomersDb = require('../persistence/CustomersDb');


module.exports.deleteSession = function deleteSession (req, res, next) {
  Session.deleteSession(req.swagger.params, res, next, CustomersDb);
};

module.exports.postSession = function postSession (req, res, next) {
  Session.postSession(req.swagger.params, res, next, CustomersDb);
};
