'use strict';

var url = require('url');


var Customers = require('./CustomersService');
var CustomersDb = require('../persistence/CustomersDb');


module.exports.deleteCustomersCustomerid = function deleteCustomersCustomerid (req, res, next) {
  Customers.deleteCustomersCustomerid(req.swagger.params, res, next, CustomersDb);
};

module.exports.getCustomers = function getCustomers (req, res, next) {
  Customers.getCustomers(req.swagger.params, res, next, CustomersDb);
};

module.exports.getCustomersCustomerid = function getCustomersCustomerid (req, res, next) {
  Customers.getCustomersCustomerid(req.swagger.params, res, next, CustomersDb);
};

module.exports.getCustomersMe = function getCustomersMe (req, res, next) {
  Customers.getCustomersMe(req.swagger.params, res, next, CustomersDb);
};

module.exports.postCustomers = function postCustomers (req, res, next) {
  Customers.postCustomers(req.swagger.params, res, next, CustomersDb);
};

module.exports.postCustomersActivate = function postCustomersActivate (req, res, next) {
  Customers.postCustomersActivate(req.swagger.params, res, next, CustomersDb);
};

module.exports.putCustomersCustomerid = function putCustomersCustomerid (req, res, next) {
  Customers.putCustomersCustomerid(req.swagger.params, res, next, CustomersDb);
};
